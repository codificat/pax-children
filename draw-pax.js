
/* Global variables */
var paxdata;    // The whole data set
var years;      // List of years we cover
var regions;    // Regions that are in the dataset
var conflicts;  // Types of conflict that are in the dataset
var stages;     // Agreement stages that are in the dataset

var childRefs;  // Agreement data split by level of references (GCh field)

/* Color palette used in the visualization */
var solarized = {
    'base03': '#002b36',
    'base02': '#073642',
    'base01': '#586e75',
    'base00': '#657b83',
    'base0': '#839496',
    'base1': '#93a1a1',
    'base2': '#eee8d5',
    'base3': '#fdf6e3',
    'yellow': '#b58900',
    'orange': '#cb4b16',
    'red': '#dc322f',
    'magenta': '#d33682',
    'violet': '#6c71c4',
    'blue': '#268bd2',
    'cyan': '#2aa198',
    'green': '#859900',
}

/* FIXME: this could be much simpler. It was made like this when testing with
   custom colors per bar */
var regionColors = {};
function defineRegionColors() {
    let colors = [
        solarized.blue,
        solarized.blue,
        solarized.blue,
        solarized.blue,
        solarized.blue,
        solarized.blue
    ];
    for (var i=0; i<regions.length; i++) {
        regionColors[regions[i]] = colors[i];
    }
}
var conflictColors = {};
function defineConflictColors() {
    let colors = [
        solarized.green,
        solarized.green,
        solarized.green,
        solarized.green
    ];
    for (var i=0; i<conflicts.length; i++) {
        conflictColors[conflicts[i]] = colors[i];
    }
}
var stageColors = {};
function defineStageColors() {
    let colors = [
        solarized.cyan,
        solarized.cyan,
        solarized.cyan,
        solarized.cyan,
        solarized.cyan,
        solarized.cyan,
        solarized.cyan
    ];
    for (var i=0; i<stages.length; i++) {
        stageColors[stages[i]] = colors[i];
    }
}

/* Common Plotly configurations applicable to all plots */
var commonLayout = {
    paper_bgcolor: 'transparent',
    plot_bgcolor: 'transparent'
};
var plotConfig = {
    displayModeBar: false,
    responsive: true
};
var barNames = ['No reference', 'Rethoric', 'Anti-discrimination', 'Substantial'];

/* References to document elements */
var overviewPlot = document.getElementById('overview');
var regionsPlot = document.getElementById('regions');
var conflictsPlot = document.getElementById('conflicts');
var stagesPlot = document.getElementById('stages');
var genComments = document.getElementById('obs-gen');
var regComments = document.getElementById('obs-reg');
var typeComments = document.getElementById('obs-type');
var stageComments = document.getElementById('obs-stage');
var agrList = document.getElementById('agrlist');
var agrSelect = Plotly.d3.select('#agrlist');

/* Keeping state */
var activeOverview = 0; // Which button / display mode is active (% or Num)
var visibleOverview = [true, true, true, true]; // Keep track of disabled levels
var selectedRegion;   // Which region has been selected
var selectedConflict; // Which conflict type has been selected
var selectedStage;    // Which conflict stage has been selected

/* Helper functions */

/* Generate a link for an agreement */
function agtLink(a) {
    var url = "https://peaceagreements.org/view/" + a['Id'];
    var linkText = '<a href="'+ url + '">';
    linkText += a['Agt'] + '</a>';
    return linkText;
}

/* Functions that draw each of the plots */

/* Draw the overview plot with agreements per year with % of child refs */
function drawOverview(baseColor = solarized.magenta) {
    let data = [{
        type: 'bar',
        x: Object.keys(_.countBy(childRefs[0], 'Y')),
        y: Object.values(_.countBy(childRefs[0], 'Y')),
        name: barNames[0],
        visible: visibleOverview[0],
        marker: {
            color: solarized.base03,
            opacity: 0.2
        },
        findme: 0
    }, {
        type: 'bar',
        x: Object.keys(_.countBy(childRefs[1], 'Y')),
        y: Object.values(_.countBy(childRefs[1], 'Y')),
        name: barNames[1],
        name: 'Rethoric',
        visible: visibleOverview[1],
        marker: {
            color: baseColor,
            opacity: 0.5
        },
        findme: 1
    }, {
        type: 'bar',
        x: Object.keys(_.countBy(childRefs[2], 'Y')),
        y: Object.values(_.countBy(childRefs[2], 'Y')),
        name: barNames[2],
        name: 'Anti-discrimination',
        visible: visibleOverview[2],
        marker: {
            color: baseColor,
            opacity: 0.7
        },
        findme: 2
    }, {
        type: 'bar',
        x: Object.keys(_.countBy(childRefs[3], 'Y')),
        y: Object.values(_.countBy(childRefs[3], 'Y')),
        name: barNames[3],
        visible: visibleOverview[3],
        marker: {
            color: baseColor,
            opacity: 1
        },
        findme: 3
    }];

    let layout = _.clone(commonLayout);
    layout.title = "References to <span>Children</span> in Peace Agreements";
    layout.barmode = 'stack';
    layout.hovermode = 'closest';
    layout.barnorm = activeOverview ? '' : 'percent';
    layout.showlegend = true;
    layout.legend = {
        'orientation': 'h',
        'itemdoubleclick': false
    };
    layout.xaxis = {range: [_.first(years)-1, _.last(years)+1]};
    layout.margin = {'l': 20, 'r':20, 't': 80, 'b':80};
    layout.updatemenus=[{
        buttons: [
            {
                args: ['barnorm', 'percent'],
                label: '%',
                method: 'relayout'
            },
            {
                args: ['barnorm', ''],
                label:'Number',
                method:'relayout'
            }
        ],
        active: activeOverview,
        //pad: {'r': 10, 't': 10},
        direction: 'right',
        showactive: true,
        type: 'buttons',
        y: -0.2,
        x: 1,
        xanchor: 'right',
        yanchor: 'bottom'
    }]

    Plotly.newPlot(overviewPlot, data, layout, plotConfig);

    overviewPlot.on('plotly_click', function(eventData){
        var ref = eventData.points[0].data.findme;
        var year = eventData.points[0].x;

        var selected = _.filter(childRefs[ref], {'Y': year});
        var num = selected.length;

        var intro = '<p>In ' + year;
        if (num === 1) {
            intro += " there was one agreement";
        } else {
            intro += " there were " + num + " agreements";
        }
        if (selectedRegion) {
            intro += ' in <span class="region">' + selectedRegion + '</span>';
        }
        if (selectedConflict) {
            intro += ' of type <span class="type">' + selectedConflict + '</span>';
        }
        if (selectedStage) {
            intro += ' at stage <span class="stage">' + selectedStage + '</span>';
        }
        intro += ' with <span class="children">' + barNames[ref];
        if (ref > 0) {
            intro += ' references to children</span>';
        } else {
            intro += ' to children</span>';
        }
        /* Avoid long lists */
        if (num > 10) {
            intro += ' (showing 10)';
            selected = _.slice(selected, 0, 10);
        }
        intro += ':</p>';
        agrList.innerHTML = intro;

        /* Add the list of links to each agreement (d3) */
        agrSelect.append("ul").selectAll("li")
            .data(selected).enter()
            .append("li").html(a => agtLink(a));
    });

    overviewPlot.on('plotly_buttonclicked', function(eventData){
        activeOverview = eventData.active;
    });

    overviewPlot.on('plotly_legendclick', function(eventData){
        // Keep track of what markers have been enabled/disabled.
        // In case the current view misses data of some type,
        // make sure there are no falses
        visibleOverview = eventData.fullData.map(a => a.visible ? a.visible : "legendonly");
        // flip the marker that was just selected
        var m = eventData.curveNumber;
        visibleOverview[m] = visibleOverview[m] === "legendonly" ? true : "legendonly";
    });
}

/* Draw the regions plot */
function drawRegions() {
    let data = [{
        type: 'bar',
        y: regions,
        x: Object.values(_.countBy(paxdata, 'Reg')),
        orientation: 'h',
        text: regions,
        textposition: 'auto',
        hoverinfo: 'none',
        marker: {
            color: Object.values(regionColors)
        }
    }];

    let layout = _.clone(commonLayout);
    layout.title = {'text':'Region', 'font': {'color':solarized.blue }};
    layout.yaxis = {'visible': false};
    layout.margin = {'l': 20, 'r':20, 't': 30, 'b':40};

    Plotly.newPlot(regionsPlot, data, layout, plotConfig);

    /* Event handling */
    regionsPlot.on('plotly_click', function(data){
        clearAgrList();

        let regionNum = data.points[0].pointNumber;
        let region = regions[regionNum];

        if (region === selectedRegion) {
            // User clicked on the region we have currently selected.
            // This unselects all regions
            resetRegion();
        } else {
            // A region has been selected
            resetConflict();
            resetStage();
            genComments.style.visibility = "hidden";
            stageComments.style.visibility = "hidden";
            typeComments.style.visibility = "hidden";
            regComments.style.visibility = "visible";
            selectedRegion = region;
            childRefs = _(paxdata).filter({'Reg':region}).groupBy('GCh').value()
            drawOverview(regionColors[region]);
            var update = { 'marker.opacity': [[]] };
            for (i=0; i<regions.length; i++) {
                if (i != regionNum) {
                    update['marker.opacity'][0].push(0.3);
                } else {
                    update['marker.opacity'][0].push(1);
                }
            }
            Plotly.restyle(regionsPlot, update);
        }
    });
}

function drawConflicts() {
    let data = [{
        type: 'bar',
        y: conflicts,
        x: Object.values(_.countBy(paxdata, 'Contp')),
        orientation: 'h',
        text: conflicts,
        textposition: 'auto',
        hoverinfo: 'none',
        marker: {
            color: Object.values(conflictColors)
        }
    }];

    let layout = _.clone(commonLayout);
    layout.title = {'text':'Conflict Type', 'font': {'color':solarized.green }};
    layout.yaxis = {'visible': false};
    layout.margin = {'l': 20, 'r':20, 't': 30, 'b':40};

    Plotly.newPlot(conflictsPlot, data, layout, plotConfig);

    /* Event handling */
    conflictsPlot.on('plotly_click', function(data){
        clearAgrList();

        let conflictNum = data.points[0].pointNumber;
        let conflict = conflicts[conflictNum];

        if (conflict === selectedConflict) {
            // User clicked on the conflict we have currently selected.
            // This unselects all conflict types
            resetConflict();
        } else {
            resetRegion();
            resetStage();
            genComments.style.visibility = "hidden";
            regComments.style.visibility = "hidden";
            stageComments.style.visibility = "hidden";
            typeComments.style.visibility = "visible";
            selectedConflict = conflict;
            childRefs = _(paxdata).filter({'Contp':conflict}).groupBy('GCh').value()
            drawOverview(conflictColors[conflict]);
            var update = { 'marker.opacity': [[]] };
            for (i=0; i<conflicts.length; i++) {
                if (i != conflictNum) {
                    update['marker.opacity'][0].push(0.3);
                } else {
                    update['marker.opacity'][0].push(1);
                }
            }
            Plotly.restyle(conflictsPlot, update);
        }
    });
}

/* Draw the stages plot */
function drawStages() {
    let data = [{
        type: 'bar',
        y: stages,
        x: Object.values(_.countBy(paxdata, 'Stage')),
        orientation: 'h',
        text: stages,
        textposition: 'auto',
        hoverinfo: 'none',
        marker: {
            color: Object.values(stageColors)
        }
    }];

    let layout = _.clone(commonLayout);
    layout.title = {'text':'Agreement Stage', 'font': {'color':solarized.cyan }};
    layout.yaxis = {'visible': false};
    layout.margin = {'l': 20, 'r':20, 't': 30, 'b':40};

    Plotly.newPlot(stagesPlot, data, layout, plotConfig);

    /* Event handling */
    stagesPlot.on('plotly_click', function(data){
        clearAgrList();

        let stageNum = data.points[0].pointNumber;
        let stage = stages[stageNum];

        if (stage === selectedStage) {
            // User clicked on the stage we have currently selected.
            // This unselects all stages
            resetStage();
        } else {
            resetRegion();
            resetConflict();
            genComments.style.visibility = "hidden";
            regComments.style.visibility = "hidden";
            typeComments.style.visibility = "hidden";
            stageComments.style.visibility = "visible";
            selectedStage = stage;
            childRefs = _(paxdata).filter({'Stage':stage}).groupBy('GCh').value()
            drawOverview(stageColors[stage]);
            var update = { 'marker.opacity': [[]] };
            for (i=0; i<stages.length; i++) {
                if (i != stageNum) {
                    update['marker.opacity'][0].push(0.3);
                } else {
                    update['marker.opacity'][0].push(1);
                }
            }
            Plotly.restyle(stagesPlot, update);
        }
    });
}

// Load the data and draw
Plotly.d3.json("pax.json",
    function(err, input) {
        // Initialize global vars with the data
        paxdata = input;

        years = Object.keys(_.countBy(paxdata, 'Y')).map(Number);
        childRefs = _.groupBy(paxdata, 'GCh');
        regions = Object.keys(_.groupBy(paxdata, 'Reg'));
        conflicts = Object.keys(_.countBy(paxdata, 'Contp'));
        _.remove(conflicts, d=>d==="Other"); // There is just 1 aggreement of 'Other' type and it's not representative
        stages = Object.keys(_.countBy(paxdata, 'Stage'));
        _.remove(stages, d=>d==="Oth"); // There 1 aggreement of 'Other' type and it's not representative
        defineRegionColors();
        defineConflictColors();
        defineStageColors();

        // Draw the graphs
        drawOverview();
        drawRegions();
        drawConflicts();
        drawStages();
});

function resetRegion() {
    if (selectedRegion == null) { return; }

    regComments.style.visibility = "hidden";
    genComments.style.visibility = "visible";

    var update = { 'marker.opacity': [[]] };
    selectedRegion = null;
    childRefs = _.groupBy(paxdata, 'GCh');
    drawOverview();
    for (i=0; i<regions.length; i++) {
        update['marker.opacity'][0].push(1);
    }
    Plotly.restyle(regionsPlot, update);
}
function resetConflict() {
    if (selectedConflict == null) { return; }

    typeComments.style.visibility = "hidden";
    genComments.style.visibility = "visible";

    var update = { 'marker.opacity': [[]] };
    selectedConflict = null;
    childRefs = _.groupBy(paxdata, 'GCh');
    drawOverview();
    for (i=0; i<conflicts.length; i++) {
        update['marker.opacity'][0].push(1);
    }
    Plotly.restyle(conflictsPlot, update);
}
function resetStage() {
    if (selectedStage == null) { return; }

    stageComments.style.visibility = "hidden";
    genComments.style.visibility = "visible";

    var update = { 'marker.opacity': [[]] };
    selectedStage = null;
    childRefs = _.groupBy(paxdata, 'GCh');
    drawOverview();
    for (i=0; i<stages.length; i++) {
        update['marker.opacity'][0].push(1);
    }
    Plotly.restyle(stagesPlot, update);
}

function clearAgrList() {
    agrSelect.html('<p class="tip">Click on a column section to see a list of agreements.</p>');
}
