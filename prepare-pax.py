#!/usr/bin/env python

import pandas as pd

df = pd.read_csv('pax_all_agreements_data.csv', parse_dates=['Dat'])

# Reshape the data to make it more suited for the visualization

# Computed columns
df['Y'] = df['Dat'].dt.year

# Rename columns
df.rename(columns={'AgtId': 'Id'}, inplace=True)

# Truncate agreement names
# Note: not using str.slice_replace because it adds repl to shorter strings
# df['Agt'] = df['Agt'].str.slice_replace(start=100, repl='…')
df['Agt'] = df['Agt'].apply(lambda x: x if len(x)<100 else x[:100]+'…')

# Group stages in 3 categories
df['Stage'].replace({'Pre':'Initial'}, inplace=True)
df['Stage'].replace({'Cea':'Ceasefire'}, inplace=True)
df['Stage'].replace({'Imp':'Implement', 'Ren':'Implement'}, inplace=True)
df['Stage'].replace({'SubPar':'Framework', 'SubComp':'Framework'}, inplace=True)

# Shorten region name
df['Reg'].replace({'Middle East and North Africa': 'Mid East & North Africa'}, inplace=True)

# Build the output dataset

# Daframe with just the columns we're interested in
data = df[['Id','Agt','Reg','Contp','Stage','GCh','Y']]
print(f'Dataset size (records, fields): {data.shape}')

# Export dataset in JSON format
data.to_json('pax.json', orient='records')
